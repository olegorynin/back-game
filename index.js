import {config} from './config.js'

document.body.onload = generateMainPage


let url = config.host + ':' + config.port
//
// let ctx = canv.getContext('2d')
//
// function createMove(direction) {
//     return function move() {
//         let response = fetch(url + '/' + direction)
//         response.then((data) => {
//             let text = data.text()
//             text.then((textData) => {
//                 let coords = JSON.parse(textData)
//                 area.value += textData
//                 ctx.clearRect(0,0, 300,300)
//                 ctx.fillStyle = 'rgb(200, 0, 0)';
//                 ctx.fillRect(coords.x * 10, coords.y * 10, 10, 10);
//
//                 console.log(textData)
//             })
//         })
//     }
// }
//
//
// btnUp.onclick = createMove('up')
// btnDown.onclick = createMove('down')
// btnLeft.onclick = createMove('left')
// btnRight.onclick = createMove('right')
function okEvent(data) {
    let text = data.text()
    text.then((textData) => {
        if (textData === "+"){
            generateGamePage()
        }
        else  if(textData === "-"){
            alert("bb")
        }
    })
}

function btnOkClick() {
    let response = fetch(url + '/ok')
    response.then(okEvent)
    // response.catch()
}

function btnRegisterClick() {
    // let response = fetch(url + '/register')

    const login = document.getElementById("login");
    const password = document.getElementById("password");

    const loginText = login.value
    const passwordText = password.value

    let registerForm = {
        login: loginText,
        password: passwordText
    }

    let response =  fetch(url + '/register', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(registerForm)
    });

    response.then((data)=>{
        console.log(data)
    })
    // response.catch()
}

function generateMainPage() {

    const app = document.getElementById("app");
    app.innerHTML = ""
    let loginInput = document.createElement("input");
    loginInput.id = "login"
    app.appendChild(loginInput)
    let passwordInput = document.createElement("input");
    passwordInput.id = "password"
    app.appendChild(passwordInput)
    let buttonOk = document.createElement("button")
    buttonOk.innerText = "Ok"
    buttonOk.onclick = btnRegisterClick
    app.appendChild(buttonOk)


}

function generateGamePage() {
    const app = document.getElementById('app')
    app.innerHTML = ""
    let canvas = document.createElement("canvas")
    canvas.width = 150
    canvas.height = 150
    let buttonUp = document.createElement("button")
    buttonUp.innerText = "Up"
    app.appendChild(canvas)
    app.appendChild(buttonUp)

}

